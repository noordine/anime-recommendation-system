import pandas as pd
import numpy as np

from scipy.sparse import csr_matrix
from sklearn.neighbors import NearestNeighbors

import matplotlib.pyplot as plt
import seaborn as sns

anime = pd.read_csv("./dataset/anime.csv")
ratings = pd.read_csv("./dataset/ratings.csv")

ratings.head()

anime.head()

final_dataset = ratings.pivot(index='animeId',columns='userId',values='rating')
final_dataset.head()

final_dataset.fillna(0,inplace=True)
final_dataset.head()

no_user_voted = ratings.groupby('animeId')['rating'].agg('count')
no_anime_voted = ratings.groupby('userId')['rating'].agg('count')

f,ax = plt.subplots(1,1,figsize=(16,4))
# ratings['rating'].plot(kind='hist')
plt.scatter(no_user_voted.index,no_user_voted,color='mediumseagreen')
plt.axhline(y=10,color='r')
plt.xlabel('AnimeId')
plt.ylabel('No. of users voted')
plt.show()

final_dataset = final_dataset.loc[no_user_voted[no_user_voted > 10].index,:]

f,ax = plt.subplots(1,1,figsize=(16,4))
plt.scatter(no_anime_voted.index,no_anime_voted,color='mediumseagreen')
plt.axhline(y=50,color='r')
plt.xlabel('UserId')
plt.ylabel('No. of votes by user')
plt.show()

final_dataset=final_dataset.loc[:,no_anime_voted[no_anime_voted > 50].index]
final_dataset

csr_data = csr_matrix(final_dataset.values)
final_dataset.reset_index(inplace=True)

knn = NearestNeighbors(metric='cosine', algorithm='brute', n_neighbors=20, n_jobs=-1)

knn = NearestNeighbors(metric='cosine', algorithm='brute', n_neighbors=20, n_jobs=-1)
knn.fit(csr_data)
    
def get_anime_recommendation(anime_name):
    n_anime_to_reccomend = 10
    anime_list = anime[anime['title'].str.contains(anime_name)]  
    if len(anime_list):        
        anime_idx= anime_list.iloc[0]['animeId']
        anime_idx = final_dataset[final_dataset['animeId'] == anime_idx].index[0]
        
        distances , indices = knn.kneighbors(csr_data[anime_idx],n_neighbors=n_anime_to_reccomend+1)    
        rec_anime_indices = sorted(list(zip(indices.squeeze().tolist(),distances.squeeze().tolist())),\
                               key=lambda x: x[1])[:0:-1]
        
        recommend_frame = []
        
        for val in rec_anime_indices:
            anime_idx = final_dataset.iloc[val[0]]['animeId']
            idx = anime[anime['animeId'] == anime_idx].index
            recommend_frame.append({'Title':anime.iloc[idx]['title'].values[0],'Distance':val[1]})
        df = pd.DataFrame(recommend_frame,index=range(1,n_anime_to_reccomend+1))
        return df
    
    else:
        
        return "No anime found. Please check your input"
